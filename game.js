function loadimages(){
    playerimage=new Image;
    playerimage.src="assets/pika.png";
    goalimage =new Image;
    goalimage.src="assets/ball.png";
    enemyimage =new Image;
    enemyimage.src="assets/gengar.png";
     /* var enemynames=["drowsy","gengar","bulbasaur"];
    
    enemyImages=[];
    for(var i=0;i<enemynames.length;i++)
        {
            enemyimg=new Image;
            enemyimg.src="assets/"+ enemynames[i] +".png";
            enemyImages.push(enemyimg);
        }*/
}
function init(leve){
    console.log("INSIDE INIT");
    mycanvas=document.getElementById("mycanvas");
     W=mycanvas.width;
     H=mycanvas.height;
    GAME_OVER=false;
    score=0;
    level=leve;
    pen=mycanvas.getContext('2d');
    enemy1={
        x:150,
        y:150,
        h:75,
        w:75,
        speedy:10,
        image:"",
        createimage:function(){
         this.image=new Image;
        this.image.src="assets/drowsy.png";
        return this.image;
    }
    };
    enemy2={
        x:350,
        y:250,
        h:75,
        w:75,
        speedy:-5,
        image:"",
        createimage:function(){
         this.image=new Image;
        this.image.src="assets/gengar.png";
        return this.image;
    }
    };
    enemy3={
        x:450,
        y:350,
        h:75,
        w:75,
        speedy:5,
        image:"",
       createimage:function(){
        this.image=new Image;
        this.image.src="assets/meow.png";
        return this.image;
    }
    };
    enemies =[];
    enemies.push(enemy1);
    enemies.push(enemy2);
    enemies.push(enemy3);
    player={
        x:0,
        y:H/2,
        h:75,
        w:75,
        speedx:0
    };
    goal={
        x:W-75,
        y:H/2,
        h:75,
        w:75
     }
    mycanvas.addEventListener('mousedown',moveplayer);
    mycanvas.addEventListener('mouseup',stoplayer);
}
function moveplayer(){
    player.speedx=10;
}
function stoplayer(){
    player.speedx=0;
}
function iscolliding(r1,r2)
{
    var first=Math.abs(r1.x-r2.x)<=r1.w-10;
    var sec=Math.abs(r1.y-r2.y)<=r1.h-10;
    if(first && sec)
        {
            return true;
        }
    else 
        {
            return false;
        }
}
function draw(){
    console.log("INSIDE DRAW");
    pen.clearRect(0,0,W,H);
    for(var i=0;i<level;i++)
        
    {
        pen.drawImage(enemies[i].createimage(),enemies[i].x,enemies[i].y,enemies[i].h,enemies[i].w);
    }
    pen.drawImage(playerimage,player.x,player.y,player.h,player.w);
    
    pen.drawImage(goalimage,goal.x,goal.y,goal.h,goal.w);
    pen.font="20px Arial";
    pen.fillStyle="white";
    pen.fillText("Score :"+score,20,20);
}
function update(){
    console.log("INSIDE UPDATE");
    for(var i=0;i<level;i++)
    {
        if(enemies[i].y>=(H-enemies[i].h) || enemies[i].y<=0)
    {
       enemies[i].speedy*=-1;        
    }
    enemies[i].y+=enemies[i].speedy; 
    
    if(iscolliding(player,enemies[i]))
        {
            console.log("player and enemy collided");
            alert("GAME OVER");
            GAME_OVER=true;
        }
    }
    player.x+=player.speedx;
    if(iscolliding(player,goal))
        {
            console.log("player reached the goal");
            alert("CONGRATS FOR CLEARING THE LEVEL "+level);
            GAME_OVER=true;
            level=(level+1);
            if(level>3)
                {
                    level=level%3;
                }
        }
    score=Math.round((player.x)/10);
}
function gameloop(){
    draw();
    update();
    if(GAME_OVER==false)
    {
        window.requestAnimationFrame(gameloop);
    }
    else{
        choice=prompt("DO YOU WANT TO PLAY AGAIN");
        if(choice=="YES" ||choice=="yes" ||choice=="Y")
            {
                restartgame(level);
            }
        else
            {
                alert("THANKS FOR PLAYING");
            }
    }
}
loadimages();
function restartgame(level)
{
    init(level);
    gameloop();
}
restartgame(level=1);    